CREATE USER rentaluser WITH PASSWORD 'rentalpassword';
GRANT CONNECT ON DATABASE dvdrental TO rentaluser;


GRANT SELECT ON customer TO rentaluser;
SELECT * FROM customer;


CREATE GROUP rental;
GRANT rental TO rentaluser;

GRANT INSERT, UPDATE ON rental TO rental;


INSERT INTO rental (rental_date, inventory_id, customer_id, return_date, staff_id, last_update)
VALUES (NOW(), 2, 2, NOW(), 2, NOW());

UPDATE rental SET return_date = NOW() WHERE rental_id = 1;

REVOKE INSERT ON rental FROM rental;



CREATE ROLE client_MARY_SMITH;
GRANT USAGE ON SCHEMA public TO client_MARY_SMITH;
GRANT SELECT ON payment TO client_MARY_SMITH;
GRANT SELECT ON rental TO client_MARY_SMITH;

SET ROLE client_MARY_SMITH;
SELECT * FROM payment WHERE customer_id = 1;
SELECT * FROM rental WHERE customer_id = 1;
RESET ROLE;

SELECT * FROM customer;

